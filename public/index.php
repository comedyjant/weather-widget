<?php
require '../vendor/autoload.php';

$conf = [
    'settings' => [
        'displayErrorDetails' => false,
    ],
];

$app = new \Slim\App($conf);

include('../src/routes.php');

$app->run();