(function(){
    var data;

    var createUrl = function(url) {
        return 'get-data?url=' + encodeURIComponent(url);
    }

    function getData(url) {
        $('#svg-container').hide();
        $('#loading-info').show();
        $.ajax({
            url: createUrl(url),
            cache: false
        }).done(function(response) {
            data = response;
            drawGraph('tmax');
            $('#svg-container').show();
            $('#loading-info').hide();            
        });
    }

    $('form#form').submit(function(e){
        e.preventDefault();
        $("#content-type").val('tmax');
        getData($(this).find('input#url-input').val());       
    });

    $('select#content-type').change(function(e) {
        drawGraph($(this).val());
    });

    function setCanvasWidth() {
        $('#svg-canvas').attr('width', $('#svg-container').width());
    }

    $('.labels .l').hover(function() {
        $('.line').addClass('disabled');
        $('.line.line-' + $(this).data('item')).removeClass('disabled').addClass('active');
    }, function() {
        $('.line').removeClass('disabled active');
    });

    function init () {
        currUrl = $('input#url-input').val();
        setCanvasWidth();
        getData($('input#url-input').val()); 
    }

    var colors = ['#808000', '#3c6404', '#495a81', '#e6194b', '#f032e6', '#800000', '#000080', '#aa6e28', '#f58231', '#0082c8', '#008080', '#3cb44b'];

    function drawGraph(type) {
        if (data === undefined) {
            alert('Data is not loaded');
            return;
        }

        d3.select("svg > *").remove();
        var svg = d3.select("svg"),
            margin = {top: 70, right: 30, bottom: 50, left: 50},
            width = svg.attr("width") - margin.left - margin.right,
            height = svg.attr("height") - margin.top - margin.bottom,
            g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        var parseTime = d3.timeParse("%Y");
        var x = d3.scaleTime().range([0, width]),
            y = d3.scaleLinear().range([height, 0]),
            z = d3.scaleOrdinal(d3.schemeCategory10);

        var line = d3.line()
                .curve(d3.curveCatmullRom)
                .x(function(d) { return x(d.year); })
                .y(function(d) { return y(d.data || 0); });

        var months = data[0].slice(1).map(function(id, i) {
            return {
              id: id,
              item: i,
              values : data.slice(1).map(function(d) {
                return {year: parseTime(d['year']), data: d['data'][type][i]}
              })
            };
        });

        x.domain(d3.extent(data.slice(1), function(d) { return parseTime(d['year']); }));

        y.domain([
            d3.min(months, function(c) { return d3.min(c.values, function(d) { return parseFloat(d.data); }); }),
            d3.max(months, function(c) { return d3.max(c.values, function(d) { return parseFloat(d.data); }); }),
        ]);

        g.append("g")
            .attr("class", "axis axis--x")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x).ticks(20));

        g.append("g")
              .attr("class", "axis axis--y")
              .call(d3.axisLeft(y)); 

        var month = g.selectAll(".month")
            .data(months)
            .enter().append("g")
            .attr("class", "month");

        month.append("path")
            .attr("class", function(d) { return 'line line-' + d.item; })
            .attr("d", function(d) { return line(d.values); })
            .style("stroke", function(d) { return colors[d.item]; });        
    }

    init();
    
})();
