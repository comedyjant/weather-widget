## Weather test project

<h2>Installation</h2>

- install composer - https://getcomposer.org/
- go to the project folder
- git clone https://gitlab.com/comedyjant/weather-widget.git .
- run command - "composer install"
- change project folder in the server config file - "_project-folder_/public"
