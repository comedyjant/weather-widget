<?php

namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class UploadController {

    private $data = [["year", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]];

    public function getData(Request $request, Response $response) {
        $url = $request->getParam('url');
        $fileContent = $this->uploadFromUrl($url);
        $this->parseContent($fileContent);
        return $response->withJson($this->data);
    }

    private function uploadFromUrl($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, false);
        curl_setopt($ch, CURLOPT_REFERER, "http://www.xcontest.org");
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    private function parseContent($content) {
        $contentLines = explode("\r\n", $content);        
        foreach($contentLines as $line) {            
            $this->fillData($this->parseLine($line));
        }
    }

    private function fillData($lineData) {
        if(!is_null($lineData)) {
            if ($this->data[count($this->data) - 1]['year'] != $lineData[0]) {
                $this->data[] = $this->createEmptyItem($lineData[0]);
            }  
             
            if($lineData[1] > 0 && $lineData[1] <= 12) {
                $this->data[count($this->data) - 1]['data']['tmax'][$lineData[1] - 1] = $lineData[2];
                $this->data[count($this->data) - 1]['data']['tmin'][$lineData[1] - 1] = $lineData[3];
                $this->data[count($this->data) - 1]['data']['af'][$lineData[1] - 1] = $lineData[4];
                $this->data[count($this->data) - 1]['data']['rain'][$lineData[1] - 1] = $lineData[5];
                $this->data[count($this->data) - 1]['data']['sun'][$lineData[1] - 1] = $lineData[6];
            }            
        }        
    }

    private function createEmptyItem($year) {
        return [
            'year' => $year,
            'data' => [
                'tmax'  => array_fill(0, 11, "0"),
                'tmin'  => array_fill(0, 11, "0"),
                'af'    => array_fill(0, 11, "0"),
                'rain'  => array_fill(0, 11, "0"),
                'sun'   => array_fill(0, 11, "0")
            ]
        ];
    }

    private function parseLine($line) {
        $line = trim($line);
        $line = preg_replace('!\s+!', ' ', $line);
        $line = preg_replace('![\*\#]!', '', $line);
        $line = preg_replace('!---!', '0', $line);
        $data = explode(' ', $line);
        if(preg_match('!^[0-9]{4}$!', $data[0])) {
            return $data;
        }
        return null;
    }
}