<?php

namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Slim\Views\PhpRenderer;

class PageController {
    
    public function home(Request $request, Response $response) {
        $view = new PhpRenderer(__DIR__.'/../../views');
        return $view->render($response, "index.html");
    }    
}